//
//  OpenWeatherService.swift
//  OpenWeatherByRB
//
//  Created by Rui Bordadagua on 16/04/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//

import Foundation
import UIKit.UIImage

public class OpenWeatherService{
  
  //MARK: - Properties
  private let apiKey = "bea8c350733cab4a7828abb8c1d81b4a"
  private let dispatchGroup = DispatchGroup()
  
  
  //MARK: - Services
  func weatherForCity(cityID: String, completionHandler: @escaping (OpenWeather?,Error?) -> () ){
    if let url = URL(string:"https://api.openweathermap.org/data/2.5/forecast?id=\(cityID)&APPID=\(apiKey)&units=metric"){
      URLSession.shared.dataTask(with: url) { data,response, error in
        if let data = data {
          do {
            let openWeather = try JSONDecoder().decode(OpenWeather.self, from: data)
            completionHandler(openWeather,error)
          }catch let error{
            completionHandler(nil,error)
          }
        }
      }.resume()
    }
  }
  
  func fetchWeatherIcons(list: [DailyWeatherInformation], completionHandler: @escaping () -> ()){
    for daily in list{
      for hourly in daily.list {
        self.dispatchGroup.enter()
        
        self.imageFor(iconName: hourly.icon, completionHandler: { image,error in
          if let error = error {
            print("Error downloading icon. \(error)")
          } else {
            hourly.image = image
          }
          self.dispatchGroup.leave()
        })
      }
    }
    dispatchGroup.notify(queue: .main, execute: {
      completionHandler()
    })
    
  }
  
  
  private func imageFor(iconName: String, completionHandler: @escaping (UIImage?,Error?) -> ()){
    if let url = URL(string: "http://openweathermap.org/img/wn/\(iconName)@2x.png") {
      URLSession.shared.dataTask(with: url) { data,response, error in
        if let data = data {
          if let image = UIImage(data: data){
            completionHandler(image,error)
          } else {
            completionHandler(nil,error)
          }
        }else{
          completionHandler(nil,error)
        }
      }.resume()
    }
  }
  
  
  
  
  
}
