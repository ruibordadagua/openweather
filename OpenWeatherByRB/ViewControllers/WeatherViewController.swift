//
//  WeatherViewController.swift
//  OpenWeatherByRB
//
//  Created by Rui Bordadagua on 17/04/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//

import UIKit
import RxSwift

class WeatherViewController: UIViewController {
  
  //MARK: - Properties
  var viewModel = OpenWeatherViewModel()
  let disposeBag = DisposeBag()
  var listOfWeatherInformation: [DailyWeatherInformation] = []
  
  //MARK: - IBOutlets
  @IBOutlet weak var tableView: UITableView?
  @IBOutlet weak var currentTemperature: UILabel?
  @IBOutlet weak var currentTemperatureIcon: UIImageView?
  
  //MARK: - Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView?.delegate = self
    tableView?.dataSource = self
  }
  
  override func viewWillAppear(_ animated: Bool) {
    viewModel.forecast.asObservable().subscribe(onNext:{ list in
      DispatchQueue.main.async {
        self.listOfWeatherInformation = list
        let currentWeather = list[0].list[0]
        self.currentTemperature?.text = currentWeather.temperature
        self.currentTemperatureIcon?.image = currentWeather.image
        self.tableView?.reloadData()
      }
      
    }).disposed(by: disposeBag)
    viewModel.fecthForecast()
  }
  
}
// MARK: - TableView Delegate
extension WeatherViewController: UITableViewDelegate, UITableViewDataSource{
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return listOfWeatherInformation.count
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return listOfWeatherInformation[section].dayDescription
  }
  
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherCollection", for: indexPath) as! WeatherTableViewCell
    let daily = listOfWeatherInformation[indexPath.section]
    cell.dayInformation = daily
    return cell
   }
}
