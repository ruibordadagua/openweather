//
//  HourlyWeatherInformation.swift
//  OpenWeatherByRB
//
//  Created by Rui Bordadagua on 16/04/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//

import Foundation
import UIKit.UIImage

public class HourlyWeatherInformation: CustomStringConvertible{
  let temperature: String
  let hour: String
  let weatherDescription: String
  let icon: String
  var image: UIImage?
  
  init(temperature:String, time: String, description:String, icon: String){
    self.temperature = temperature
    self.hour = time
    self.weatherDescription = description
    self.icon = icon
  }
  
  public var description: String {
      return "(\(hour), \(temperature))"
  }
}
