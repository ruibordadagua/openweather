//
//  WeatherInformation.swift
//  OpenWeatherByRB
//
//  Created by Rui Bordadagua on 16/04/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//

import Foundation

public class DailyWeatherInformation : CustomStringConvertible{
  
  let day: Date
  let list: [HourlyWeatherInformation]
  let dayDescription: String
  
  init(day: Date,list: [HourlyWeatherInformation],dayDescription: String){
    self.day = day
    self.list = list
    self.dayDescription = dayDescription
  }
  
  public var description: String {
      return "(\(dayDescription), \(list))"
  }
  
}
