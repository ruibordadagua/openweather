//
//  HourInformationCollectionViewCell.swift
//  OpenWeatherByRB
//
//  Created by Rui Bordadagua on 16/04/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//

import UIKit

class HourInformationCollectionViewCell: UICollectionViewCell {
  
  //MARK: - Outlets
  @IBOutlet weak var temperature: UILabel?
  @IBOutlet weak var hour: UILabel?
  @IBOutlet weak var weatherImage: UIImageView?
    
}
