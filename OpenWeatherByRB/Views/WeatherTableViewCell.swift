//
//  WeatherTableViewCell.swift
//  OpenWeatherByRB
//
//  Created by Rui Bordadagua on 16/04/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {
  
  //MARK: - Outlets
  @IBOutlet weak var collectionView: UICollectionView?
  
  //MARK: - Properties
  var dayInformation: DailyWeatherInformation? {
    didSet {
      collectionView!.reloadData()
    }
  }
  //MARK: - Lifecycle
  override func awakeFromNib() {
        super.awakeFromNib()
    collectionView?.delegate = self
    collectionView?.dataSource = self
  }

}

//MARK: - CollectionView
extension WeatherTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return dayInformation!.list.count
  }
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HourInformation", for: indexPath) as! HourInformationCollectionViewCell
      if let info = dayInformation{
        cell.temperature?.text = info.list[indexPath.row].temperature
        cell.hour?.text = info.list[indexPath.row].hour
        if let image = info.list[indexPath.row].image{
          cell.weatherImage?.image = image
        }
      }
      return cell
  }
}
