//
//  ViewModel.swift
//  OpenWeatherByRB
//
//  Created by Rui Bordadagua on 16/04/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//

import Foundation
import RxSwift


public class OpenWeatherViewModel{
  
  //MARK: - Properties
  let portoOpenWeatherId = "2735943"
  var forecast = PublishSubject<[DailyWeatherInformation]>()
  let owService = OpenWeatherService()
  
  //MARK: - Lifecycle
  init(){
  }
  
  //MARK: - Functions
  func fecthForecast(){
    owService.weatherForCity(cityID: portoOpenWeatherId, completionHandler: { weatherInformation,error in
      if let info = weatherInformation {
        let list = self.prepareWeatherInformation(weatherInfo: info)
        self.owService.fetchWeatherIcons(list: list, completionHandler: {
            self.forecast.onNext(list)
        })
      }else{
        print("There was an error: \(error?.localizedDescription ?? "unknown")")
      }
    })
  }
  
  //MARK: - Helper Functions
  func prepareWeatherInformation(weatherInfo: OpenWeather) -> [DailyWeatherInformation]{
    
    let list = weatherInfo.list
    var dailyWeatherInformation : [DailyWeatherInformation] = []
    var hourlyWeatherInfo : [HourlyWeatherInformation] = []
    var currentDay = Date(timeIntervalSince1970: TimeInterval(list[0].dt))
    let calendar = Calendar.current
    
    
    for (index,element) in list.enumerated(){
      let date = Date(timeIntervalSince1970: TimeInterval(element.dt))
      
      if (!calendar.isDate(date, inSameDayAs: currentDay) || index == list.count - 1 ){
        let daily = DailyWeatherInformation(day: currentDay, list: Array(hourlyWeatherInfo), dayDescription: dayDescription(date: currentDay))
        dailyWeatherInformation.append(daily)
        hourlyWeatherInfo = []
        currentDay = date
      }
      let hour = String(calendar.component(.hour, from: date))
      let temperature = "\(Int(element.main.temp))º"
      let description = element.weather[0].weatherDescription
      let icon = element.weather[0].icon
      let hourly = HourlyWeatherInformation(temperature: temperature, time: hour, description: description, icon: icon )
      hourlyWeatherInfo.append(hourly)
    }
    return dailyWeatherInformation
  }
    
  func dayDescription(date: Date) -> String{
    let f = DateFormatter()
    let calendar = Calendar.current
    var description = ""
    if(calendar.isDateInToday(date)){
      description += "Today"
    }else if (calendar.isDateInTomorrow(date)){
      description += "Tomorrow"
    }else {
      let weekday = f.weekdaySymbols[calendar.component(.weekday, from: date) - 1]
      description += weekday
    }
    let month = f.monthSymbols[calendar.component(.month, from: date)]
    let day = calendar.component(.day, from: date)
    description += ",\(month) \(day)"
    return description
  }
    
}
