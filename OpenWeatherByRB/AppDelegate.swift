//
//  AppDelegate.swift
//  OpenWeatherByRB
//
//  Created by Rui Bordadagua on 15/04/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }


}

